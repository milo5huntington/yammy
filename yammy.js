const { ShardingManager } = require('kurasuta');
const { join } = require('path');
const sharder = new ShardingManager(join(__dirname, 'bot'), {
	token: "NOPE",
	shardCount: 1,
	respawn: true,
	guildsPerShard: 1500,
	ipcSocket: 9997
});

sharder.spawn();