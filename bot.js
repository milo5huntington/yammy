const fs = require("fs");
const { guilds } = require("./utils/variables");
process.setMaxListeners(0);
const { BaseCluster } = require('kurasuta');

module.exports = class extends BaseCluster {
	launch() {
		this.client.login("NOPE")
		this.client.once("ready", async () => {
			console.log(`Online on ${this.client.user.tag} on shard ${this.id}`);
			this.client.user.setActivity(`y!help || Droplet`);
		});

		this.client.on("shardDisconnected", (shard, id) => {
			console.log("Shard went down, id: " + id);
		});
		this.client.on("shardReconnecting", id => {
			console.log("Shard is reconnecting, shard with ID: " + id)
		})
		this.client.on("shardResumed", id => {
			console.log("Shard is resumed, shard with ID: " + id)
		})
		this.client.on("shardError", (err, id) => {
			console.log("Shard " + id + " had an error: " + err)
		})

		this.client.commands = new Map();
		this.client.aliases = new Map();

		fs.readdir('./commands/', (err, files) => {
			let jsfiles = files.filter(f => f.split('.')
				.pop() === 'js');
			if (jsfiles.length <= 0) {
				return;
			}
			jsfiles.forEach(f => {
				let props = require(`./commands/${f}`);
				props.fileName = f;
				this.client.commands.set(f.slice(0, -3), props);
				props.command.aliases.forEach(alias => {
					this.client.aliases.set(alias, f.slice(0, -3));
				});
			});
			console.log("Loaded " + jsfiles.length + " commands.")
		});

		this.client.on("message", async message => {
			if (message.author.bot) return;
			if (!message.guild) return;
			let guildObj = {
				footer: "Made by Droplet Development - https://dropletdev.com/discord",
				dms: true,
				embedColor: "0x731DD8"
			};
			guilds.set(message.guild.id, guildObj)
			let prefix = "y!";
			if (!message.content.startsWith(prefix)) return;
			const args = message.content.slice(prefix.length).trim().split(/ +/g);
			let command = args.shift().toLowerCase();
			let cmd;
			if (this.client.commands.has(command)) {
				cmd = this.client.commands.get(command);
			} else if (this.client.aliases.has(command)) {
				cmd = this.client.commands.get(this.client.aliases.get(command));
			}
			if (!cmd) return;
			cmd.use(this.client, message, args, command);

		});
	}
};