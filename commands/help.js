const { MessageEmbed } = require("discord.js");
const { guilds } = require("../utils/variables");

exports.use = async (client, message, args, command) => {

    const helpEmbed = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("Yammy Help - v1 Commands")
        .setDescription("Need more help than I can provide? Join our discord https://dropletdev.com/discord \nYammy v1 commands:\n**y!play** - Play a song\n**y!help** - You are already here..\n**y!invite** - Invite this amazing bot!\n**y!leave** - Leave the voice channel\n**y!nowplaying** - What song is playing?\n**y!pause** - Pause the song\n**y!resume** - Resume the song\n**y!skip** - Vote to skip a song!\n**y!stats** - View some stats!\n**y!support** - Get a invite link to our support server\n**y!volume** - Change the volume!\n**y!summon** - Summon the bot to you!\n**y!queue** - View the queue!")
        .setFooter(guilds.get(message.guild.id).footer);

    return message.channel.send(helpEmbed).catch(err => { });

};

exports.command = {
    aliases: [""]
};