const { admins, guilds } = require("../utils/variables");
const { MessageEmbed } = require("discord.js");
exports.use = async (client, message, args, command) => {
    if (!admins.includes(message.author.id)) return;
    function clean(text) {
        if (typeof (text) === "string")
            return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
        else
            return text;
    } try {
        var output = true;
        let code = args.join(" ");
        if (args[0].toLowerCase() == "async") code = `(async function(){\n${code.slice(5)}\n})(client, message)`;
        let evaled = await eval(code);
        let rawEvaled = evaled;
        if (typeof evaled !== "string")
            evaled = require("util").inspect(evaled, {
                "depth": 0
            });
        let dataType = Array.isArray(rawEvaled) ? "Array<" : typeof rawEvaled, dataTypes = [];
        if (~dataType.indexOf("<")) {
            rawEvaled.forEach(d => {
                if (~dataTypes.indexOf(Array.isArray(d) ? "Array" : typeof d)) return;
                dataTypes.push(Array.isArray(d) ? "Array" : typeof d);
            });
            dataType += dataTypes.map(s => s[0].toUpperCase() + s.slice(1)).join(", ") + ">";
        }
        // The Embed for the result of the EVAl
        let EvalResult = new MessageEmbed()
            .addField(":inbox_tray: Input", `\`\`\`js\n${code}\n\`\`\``)
            .addField(":outbox_tray: Output", `\`\`\`js\n${clean(evaled).replace(client.token, "Nice try.")}\n\`\`\``)
            .addField('Type', `\`\`\`xl\n${(dataType).substr(0, 1).toUpperCase() + dataType.substr(1)}\n\`\`\``)
        if (output) message.channel.send(EvalResult);
    } catch (err) {
        message.channel.send(`\`ERROR\` \`\`\`js\n${clean(err)}\n\`\`\``);
    }
};

exports.command = {
    aliases: [""]
}