const { active } = require("../utils/variables");
exports.use = async (client, message, args, command, link) => {
  if (!message.member.voice.channel) return message.reply("You have to be in a voice channel to play a song!");
  if (message.member.voice.channelID !== message.guild.me.voice.channelID) return message.reply("We are not in the same channel!");
  let fetched = active.get(message.guild.id);
  if (!fetched) return message.reply("There isn't any songs playing at this moment!");
  if (!args[0]) return message.reply("Set volume to 0, SIKE");
  if (!isNaN(args[0]) && args[0] < 200 && args[0] > 0) return message.reply("You must put in a number between 0-200");
  fetched.dispatcher.setVolume(args[0] / 100);
  message.reply("I have set the volume to " + args[0]);
};

exports.command = {
  aliases: [""]
};	