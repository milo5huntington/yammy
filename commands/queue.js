const { MessageEmbed } = require("discord.js");
const { active } = require("../utils/variables");
exports.use = async (client, message, args, command) => {

  let fetched = active.get(message.guild.id);
  if (!fetched) return message.reply("There isn't any songs playing at this moment!");

  let queue = fetched.queue;
  let nowPlaying = queue[0];

  let resp = `__**NOW PLAYING**__\n**${nowPlaying.songTitle}** -- **Requested by:** ${nowPlaying.requester}\n\n__**Queue**__\n`;

  for (let i = 1; i < queue.length; i++) {
    resp += `${i}. **${queue[i].songTitle}** -- **Requested by:** *${queue[i].requester}*\n`
  }

  message.channel.send(resp);
};

exports.command = {
  aliases: [""]
};