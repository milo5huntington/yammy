const ytdl = require("ytdl-core");
const { active } = require("../utils/variables");
exports.use = async (client, message, args, command) => {

	if (!message.member.voice.channel) return message.reply("You have to be in a voice channel to play a song!");

	if (!args[0]) return message.reply("You need a valid link for me to play!");

	let validate = await ytdl.validateURL(args[0]);
	if (!validate) {
		let commandFile = require("./search");
		return commandFile.use(client, message, args, command);
	};
	let info = await ytdl.getInfo(args[0])
	let data = active.get(message.guild.id) || {};

	if (!data.connection) data.connection = await message.member.voice.channel.join();
	if (!data.queue) data.queue = [];
	data.guildID = message.guild.id;

	data.queue.push({
		songTitle: info.title,
		requester: message.author.tag,
		url: info.video_url,
		announceChannel: message.channel.id
	});

	if (!data.dispatcher) play(client, active, data); else {
		message.reply(`Added to queue: ${info.title} | Requested by ${message.author.tag}`)
	};
	active.set(message.guild.id, data);

};

exports.command = {
	aliases: [""]
};

async function play(client, active, data) {
	client.channels.get(data.queue[0].announceChannel).send(`Now playing ${data.queue[0].songTitle} | Requested by ${data.queue[0].requester}`);
	data.dispatcher = await data.connection.play(ytdl(data.queue[0].url), { filter: 'audioonly', quality: 'best' });
	data.dispatcher.guildID = data.guildID;

	data.dispatcher.once("finish", function () {
		finish(client, active, this);
	});
}

async function finish(client, active, dispatcher) {

	let fetched = active.get(dispatcher.guildID);
	fetched.queue.shift();

	if (fetched.queue.length > 0) {
		play(client, active, fetched)
	} else {
		active.delete(dispatcher.guildID);

		let vc = client.guilds.get(dispatcher.guildID).me.voice.channel;
		if (vc) vc.leave();
	}
}