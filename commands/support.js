const { MessageEmbed } = require("discord.js");
const { guilds } = require("../utils/variables");

exports.use = async (client, message, args, command) => {

    let supportEmbed = new MessageEmbed()
        .setColor(guilds.get(message.guild.id).embedColor)
        .setTitle("Yammy - Support Server")
        .setDescription("Need help using the Yanny bot? Join our support server [here](https://dropletdev.com/discord)")
        .setFooter(guilds.get(message.guild.id).footer);

    return message.channel.send(supportEmbed).catch(err => { });


};

exports.command = {
    aliases: [""]
}