const { active } = require("../utils/variables");
exports.use = async (client, message, args, command, link) => {
	if (!message.member.voice.channel) return message.reply("You have to be in a voice channel to play a song!");
	if (message.member.voice.channelID !== message.guild.me.voice.channelID) return message.reply("We are not in the same channel!");
	let fetched = active.get(message.guild.id);
	if (!fetched) return message.reply("There isn't any songs playing at this moment!");

	if (fetched.dispatcher.resumed) return message.reply("The song is allready resumed!");
	fetched.dispatcher.resume();

	return message.reply(`Resumed **${nowPlaying.songTitle}** -- **Requested by:** ${nowPlaying.requester}`);
};

exports.command = {
	aliases: [""]
};	